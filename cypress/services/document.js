export const documentService = {
    generateDoc: "v1/generate",
    getVersion: "v1/",
    getBarcode: "v1/barcode",
    getActuator: "actuator"
}