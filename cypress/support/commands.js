Cypress.Commands.add('getCall', (url) => {
     return cy.request('GET', `${Cypress.env("testDataUrl")}/${url}`);
});

Cypress.Commands.add('postCall', (url,body) => {
    return cy.request({
        method:'POST', 
        url: `${Cypress.env("testDataUrl")}/${url}`,
        body: body,
        gzip:false
    });
});

Cypress.Commands.add('deleteCall', (url,header) => {
    return cy.request({
        method:'DELETE',
        headers: header,
        url:`${Cypress.env("testDataUrl")}/${url}`});
});

Cypress.Commands.add('putCall', (url,body,header) => {
    return cy.request({
        method:'PUT',
        headers: header,
        url: `${Cypress.env("testDataUrl")}/${url}`,
        body: body
    });
});