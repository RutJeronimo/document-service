import {Given,Then} from 'cypress-cucumber-preprocessor/steps';
import {documentService} from '../../services/document';

Given ('I get the document version', () =>{
    cy.getCall(documentService.getVersion).as('version')
});

Then ('the document service version should be displayed successfully', () =>{
    cy.get('@version').should((response) => {
        expect(response.status).to.eq(200)  
        expect(response.body).to.contain("Commerce Services Document Service");
      });
});
 
Given ('I get the document barcode {string}', (barcode) =>{
    cy.getCall(`${documentService.getBarcode}/${barcode}/GIF`).as('barcode')
});

Then ('the document barcode should be displayed successfully', () =>{
    cy.get('@barcode').should((response) => {
        expect(response.status).to.eq(200)  
        expect(response.body).to.contain("data:image/gif");
      });
});

Given ('I get the document Actuator', () =>{
    cy.getCall(documentService.getActuator).as('actuator')
});

Then ('the Actuator should be displayed successfully', () =>{
    cy.get('@actuator').should((response) => {
        expect(response.status).to.eq(200)  
        expect(response.body).to.have.property('links') 
      });
});