Feature: Get Document service details
     
    Scenario: Verify the Version
        Given I get the document version
        Then the document service version should be displayed successfully

    Scenario: Verify the Barcode
        Given I get the document barcode "0498805429650/CODE128"
        Then the document barcode should be displayed successfully

    Scenario: Verify the Actuator
        Given I get the document Actuator
