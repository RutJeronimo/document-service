Feature: Generate Documents 

    Scenario: Verify an ESCPOS Receipt created
        Given I generate "ESCPOS" receipt 
        Then the ESCPOS document should be created successfully

    Scenario: Verify a HTML Receeipt created
        Given I generate "HTML" receipt
        Then the HTML document should be created successfully 

    Scenario: Verify a JPG Receeipt created
        Given I generate "JPG" receipt
        Then the JPG document should be created successfully    

    Scenario: Verify a PNG Receeipt created
        Given I generate "PNG" receipt
        Then the PNG document should be created successfully

    Scenario: Verify a GIF Receeipt created
        Given I generate "GIF" receipt
        Then the GIF document should be created successfully 

    Scenario: Verify a PDF Receeipt created
        Given I generate "PDF" receipt
        Then the PDF document should be created successfully        
     
 