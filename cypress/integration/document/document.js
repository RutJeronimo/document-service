
import {Given} from 'cypress-cucumber-preprocessor/steps';
import {documentService} from '../../services/document';

Given ('I generate {string} receipt',  (documentType) =>{  
    cy.fixture('receiptData.json').then( (docData) =>{
        var requestBody = {
            "data": docData.data,
            "targetType": documentType,
            "template": docData.template
        }   
        cy.postCall(documentService.generateDoc, requestBody).as('doc'); 
    });     
});

Then ('the ESCPOS document should be created successfully', () =>{
    cy.get('@doc').should((response) => {
        expect(response.status).to.eq(200) 
        expect(response.body).to.have.property('printCommands')
        expect(response.body.printCommands[0]).to.not.be.empty;;
      })  
});

Then ('the HTML document should be created successfully', () =>{
    cy.get('@doc').should((response) => {
        expect(response.status).to.eq(200)  
        expect(response.body).to.contain("<!DOCTYPE html>");
      })  
});

Then ('the JPG document should be created successfully', () =>{
    cy.get('@doc').should((response) => {
        expect(response.status).to.eq(201)  
        expect(response.headers).to.have.property('content-type','image/jpeg');
      })  
});

Then ('the PNG document should be created successfully', () =>{
    cy.get('@doc').should((response) => {
        expect(response.status).to.eq(201)  
        expect(response.headers).to.have.property('content-type','image/png');
      })  
});

Then ('the GIF document should be created successfully', () =>{
    cy.get('@doc').should((response) => {
        expect(response.status).to.eq(201)  
        expect(response.headers).to.have.property('content-type','image/gif');
      })  
});

Then ('the PDF document should be created successfully', () =>{
    cy.get('@doc').should((response) => {
        expect(response.status).to.eq(201)  
        expect(response.headers).to.have.property('content-type','application/octet-stream');
        expect(response.body).to.contain("PDF");
      })  
});